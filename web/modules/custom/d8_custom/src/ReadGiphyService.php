<?php

namespace Drupal\d8_custom;

/**
 * Class ReadGiphyService.
 */
class ReadGiphyService {

  /**
   * Constructs a new ReadGiphyService object.
   */
  public function __construct() {

  }

  /**
   * Read Data from giphy.com.
   */
  public function readData($input) {
    $url = "http://api.giphy.com/v1/gifs/search?api_key=w5ggWquR3mG2Y3IJAb0xA3YxUg3xfYXx&limit=1";
    $url .= "&q=" . urlencode($input);
    $response = json_decode(file_get_contents($url));
    if (isset($response->meta) && $response->meta->status == 200) {
      $response_data = $response->data[0];
      $data = [];
      $data['embed_url'] = $response_data->embed_url;
      $renderable = [
        '#theme' => 'giphy_results',
        '#data' => $data,
      ];
      $rendered = \Drupal::service('renderer')->render($renderable);
      return $rendered;
    }
  }

}
